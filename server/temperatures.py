"""
    Implements endpoints for temperature conversion
"""

from flask import Flask, jsonify
from converter import Converter, UNITS


app = Flask(__name__)


def get_formatted_conversion(unit, temperature):
    """

    :param unit: The temperature unit to convert from
    :param temperature: The value to convert
    :return: A dictionary with units as keys and converted values
    for each unit as values
    """
    converter = Converter(unit)
    return converter.convert(temperature)


@app.route('/convert/<string:unit>/<string:temperature>', methods=['GET'],
           strict_slashes=False)
def convert(unit, temperature):
    """

    :param unit: A temperature unit to convert from
    :param temperature: The value to convert
    :return: The HTTP response with adequate response code and
    all converted values
    """
    error = ''
    if unit not in UNITS.keys():
        error = 'Unit %s is not yet handled.' % unit
    try:
        temperature = float(temperature)
        conversion = get_formatted_conversion(unit, temperature)
    except ValueError as e:
        error = e.message
    if error:
        response = jsonify({'message': error})
        response.status_code = 400
        return response
    return jsonify(conversion)


if __name__ == '__main__':
    app.run()
