"""
    Defines a temperature converter class
"""


UNITS = {
    'fahrenheit': -470,
    'celsius': -273.15,
    'kelvin': 0,
    'rankine': 0,
}

class Converter(object):
    """
    Class that contains methods to convert a temperature
    from its unit to others.

    The self.methods attributes stores a dictionary in the following form :
    {
        destination unit: callable to call to convert a temperature in this
                          unit
    }

    """

    def __init__(self, unit):
        """
        Retrieve all available conversion methods in the converter for this
        unit.

        :param unit: The origin unit from which we are converting
        """
        self.unit = unit
        # Conversion methods are in the form origin_to_destination.
        # This way we dynamically retrieve all methods that can be used to
        # convert a value from this unit.
        self.methods = {
            method.split('_to_')[1]: getattr(self, method)
            for method in dir(self)
            if callable(getattr(self, method)) and
               method.startswith(unit) and len(method.split('_to_')) == 2
        }

    def convert(self, temperature):
        """
        Checks if the temperature is a valid value and converts it with all
        implemented methods.

        :param temperature: The temperature to convert
        :return: A dictionary with the temperature converted using
        all available methods (therefore in every handled units)
        """
        if temperature < UNITS[self.unit]:
            raise ValueError('Temperature is below absolute 0')
        return {
            key: value(temperature) for key, value in self.methods.items()
        }

    @staticmethod
    def fahrenheit_to_celsius(temperature):
        """ Converts Fahrenheits to Celsius """
        return (temperature - 32) / 1.8

    @staticmethod
    def fahrenheit_to_rankine(temperature):
        """ Converts Fahrenheits to Rankine """
        return temperature + 459.67

    @staticmethod
    def fahrenheit_to_kelvin(temperature):
        """ Converts Fahrenheits to Kelvins """
        return (temperature + 459.67) / 1.8

    @staticmethod
    def celsius_to_rankine(temperature):
        """ Converts Celsius to Rankine """
        return temperature * 1.8 + 32 + 491.67

    @staticmethod
    def celsius_to_fahrenheit(temperature):
        """ Converts Celsius to Fahrenheit """
        return temperature * 1.8 + 32

    @staticmethod
    def celsius_to_kelvin(temperature):
        """ Converts Celsius to Kelvin """
        return temperature + 273.15

    @staticmethod
    def rankine_to_celsius(temperature):
        """ Converts Rankine to Celsius """
        return (temperature - 491.67) / 1.8

    @staticmethod
    def rankine_to_fahrenheit(temperature):
        """ Converts Rankine to Fahrenheit """
        return temperature - 459.67

    @staticmethod
    def rankine_to_kelvin(temperature):
        """ Converts Rankine to Kelvin """
        return temperature / 1.8

    @staticmethod
    def kelvin_to_celsius(temperature):
        """ Converts Kelvin to Celsius """
        return temperature - 273.15

    @staticmethod
    def kelvin_to_fahrenheit(temperature):
        """ Converts Kelvin to Fahrenheit """
        return temperature * 1.8 - 459.67

    @staticmethod
    def kelvin_to_rankine(temperature):
        """ Converts Kelvins to Rankine """
        return temperature * 1.8
